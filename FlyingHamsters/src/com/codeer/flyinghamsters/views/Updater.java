package com.codeer.flyinghamsters.views;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;

import com.badlogic.gdx.math.Vector2;

import android.annotation.SuppressLint;
import android.widget.Toast;

public class Updater implements IUpdateHandler {
	private boolean down = false;
	private boolean stop = false;
	private boolean showToast = true;
	private float totalTime = 0.0f;
	private Vector2 centre;
	private float r;
	private LevelActivity la;
	Hamster hamster;
	private float angleAmplitude = Double.valueOf(Math.PI / 2).floatValue();
	private float angle0 = (-1) * Double.valueOf(Math.PI / 2).floatValue();
	public Updater(Hamster hamster, float radius, Vector2 centre, LevelActivity la) {
		this.hamster = hamster;
		this.centre = centre;
		this.r = radius;
		this.la = la;
	}
	
	@Override
	public void onUpdate(float pSecondsElapsed) {
		if (!hamster.isKicked()) {
			final float period = 6f;
			final float angularFrequency = Double.valueOf(Math.PI * 2 / period).floatValue();
			totalTime += pSecondsElapsed;
			if (totalTime > period)
				totalTime-= period;
//			Harmonic movement deg(t) = Amplitude * sin((2 pi / period) * t)
			final float angle = Double.valueOf(this.angleAmplitude * Math.sin(angularFrequency * totalTime)).floatValue() + this.angle0;
			final float rotAngle = Double.valueOf(this.angleAmplitude * Math.sin(angularFrequency * totalTime)).floatValue();
			final float x = this.centre.x + Double.valueOf(r * Math.cos(angle)).floatValue();
			final float y = this.centre.y + Double.valueOf(r * Math.sin(angle)).floatValue();
			this.hamster.getBody().setTransform(x / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, 
					y / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, rotAngle);
		}
		if (this.hamster.isKicked() && !stop) {
			hamster.getBody().setTransform(hamster.getBody().getPosition().x, hamster.getBody().getPosition().y, hamster.getBody().getAngle() / 1.05f);
		}
		if (this.hamster.isKicked() && !down && !stop && hamster.getBody().getPosition().y < 4){
			hamster.getSprite().stopAnimation(5);
			down = true;
		}
		if (this.hamster.isKicked() && down && !stop && hamster.getBody().getPosition().y > 4.5){
			hamster.getSprite().stopAnimation(0);
			down = false;
		}
		if (this.hamster.isKicked() && down && Math.abs(hamster.getBody().getLinearVelocity().y) < 0.01){
			stop = true;
			hamster.getSprite().stopAnimation(4);
		}
		if (this.hamster.isKicked() && stop && showToast){
			this.la.toastOnUiThread("distance: " + Math.round(hamster.getBody().getPosition().x) +" m.", Toast.LENGTH_SHORT);
			showToast = false; 
		}
	}
	@Override
	public void reset() {
	}
}

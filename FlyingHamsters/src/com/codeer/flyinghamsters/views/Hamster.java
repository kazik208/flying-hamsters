package com.codeer.flyinghamsters.views;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.annotation.SuppressLint;
import android.widget.Toast;

import org.andengine.engine.camera.BoundCamera;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Hamster {
	private AnimatedSprite as;
	private Body b;
	private boolean kicked;
	private final float MAXV = 15f;
	private LevelActivity la;
	public Hamster(float x, float y, TiledTextureRegion ttr, VertexBufferObjectManager vbom, 
			PhysicsWorld phys, FixtureDef fxdef, BoundCamera c, LevelActivity la, Scene s) {
		this.as = new AnimatedSprite(x, y, ttr, vbom); 
		this.as.animate(new long[]{200, 200}, 2, 3, true);
		this.b = PhysicsFactory.createBoxBody(phys, this.as, BodyType.StaticBody, fxdef);
		phys.registerPhysicsConnector(new PhysicsConnector(this.as, this.b, true, true));
		c.setChaseEntity(this.as);
		this.la = la;
		s.attachChild(this.as);
	}
	protected Body getBody() {
		return this.b;
	}
	protected AnimatedSprite getSprite() {
		return this.as;
	}
	protected boolean isKicked() {
		return this.kicked;
	}
	protected void throwHamster(float angVel) {
		if (!this.kicked && this.b.getAngle() > 0) {
			float vel = angVel/3;
			vel = Math.min(Math.abs(vel), MAXV);
			final float angle = this.b.getAngle();
			final float vx = Math.abs(vel * Double.valueOf(Math.cos(angle)).floatValue());
			final float vy = Math.abs(vel * Double.valueOf(Math.sin(angle)).floatValue());
			this.la.toastOnUiThread("velocity: " + Math.round(vel/MAXV * 100) + "% angle " + Math.round(this.b.getAngle() * 180 / Math.PI) + " degrees.", Toast.LENGTH_SHORT);
			this.b.setType(BodyType.DynamicBody);
			this.b.setLinearVelocity(vx, vy);
			this.as.stopAnimation(0);
			kicked = true;
		}
	}
}

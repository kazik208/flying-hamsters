package com.codeer.flyinghamsters.views;

import java.io.IOException;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Gradient;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class LevelActivity extends SimpleBaseGameActivity {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;

	private static final int LEVEL_WIDTH = 50 * CAMERA_WIDTH;
	private static final int LEVEL_HEIGHT = 2 * CAMERA_HEIGHT;

	private static final int HAMSTER_COLUMNS = 2;
	private static final int HAMSTER_ROWS = 3;
	

	// ===========================================================
	// Fields
	// ===========================================================

	private ITexture mHamsterTexture;
	private TiledTextureRegion mHamsterTextureRegion;

	
	private ITexture mWheelTexture;
	private ITextureRegion mWheelTextureRegion;
	
	private ITexture mWheelBaseTexture;
	private ITextureRegion mWheelBaseTextureRegion;

	private ITexture mParallaxLayerMidTexture;
	private ITexture mParallaxLayerFrontTexture;

	private ITextureRegion mParallaxLayerMidTextureRegion;
	private ITextureRegion mParallaxLayerFrontTextureRegion;
	
	private BoundCamera camera;

	private ParallaxLayer mParallaxLayer;
	
//	pPhysicsWorld objects do not interact with hamster, hPhysicsWorld objects do.
	private PhysicsWorld pPhysicsWorld, hPhysicsWorld;
	
	private Hamster hamster;
   
	// ===========================================================
	// Constructors
	// ===========================================================
	private Gradient mGradient;
	private AssetBitmapTexture mParallaxLayerGroundTexture;
	private TextureRegion mParallaxLayerGroundTextureRegion;

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {
		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		camera.setBounds(-1 * CAMERA_WIDTH, 0, LEVEL_WIDTH, LEVEL_HEIGHT);
		camera.setBoundsEnabled(true);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
	}

	@Override
	public void onCreateResources() throws IOException {
		//Hamster
		this.mHamsterTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/hamster_tiled.png", TextureOptions.BILINEAR);
		this.mHamsterTextureRegion = TextureRegionFactory.extractTiledFromTexture(mHamsterTexture, HAMSTER_COLUMNS, HAMSTER_ROWS);
		this.mHamsterTexture.load();

		//Wheel
		this.mWheelTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/wheel.png", TextureOptions.BILINEAR);
		this.mWheelTextureRegion = TextureRegionFactory.extractFromTexture(this.mWheelTexture);
		this.mWheelTexture.load();
		
		this.mWheelBaseTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/wheel_base.png", TextureOptions.BILINEAR);
		this.mWheelBaseTextureRegion = TextureRegionFactory.extractFromTexture(this.mWheelBaseTexture);
		this.mWheelBaseTexture.load();
		
		//LOAD Background
		this.mParallaxLayerMidTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/mountains.png");
		this.mParallaxLayerMidTextureRegion = TextureRegionFactory.extractFromTexture(this.mParallaxLayerMidTexture);
		this.mParallaxLayerMidTexture.load();

		this.mParallaxLayerFrontTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/hills.png");
		this.mParallaxLayerFrontTextureRegion = TextureRegionFactory.extractFromTexture(this.mParallaxLayerFrontTexture);
		this.mParallaxLayerFrontTexture.load();
		
		this.mParallaxLayerGroundTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "img/ground_stripe.png");
		this.mParallaxLayerGroundTextureRegion = TextureRegionFactory.extractFromTexture(this.mParallaxLayerGroundTexture);
		this.mParallaxLayerGroundTexture.load();
	}

	private PhysicsWorld createPhysicsWorld(Scene s, Vector2 gravity, boolean allowSleep) {
		PhysicsWorld result = new PhysicsWorld(gravity, allowSleep);
		s.registerUpdateHandler(result);
		return result;
	}
	
	@Override
	public Scene onCreateScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());
		
		final VertexBufferObjectManager vertexBufferObjectManager = this.getVertexBufferObjectManager();
		final Vector2 gravity = new Vector2(.0f, -3f);
		final Scene scene = new Scene();
		
		this.hPhysicsWorld = this.createPhysicsWorld(scene, gravity, false);
		this.pPhysicsWorld = this.createPhysicsWorld(scene, gravity, false);

		mParallaxLayer = new ParallaxLayer(camera, true, LEVEL_WIDTH);
		mParallaxLayer.setParallaxChangePerSecond(0); 
		mParallaxLayer.setParallaxScrollFactor(.1f);
		
		this.mGradient = new Gradient(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2, CAMERA_WIDTH, CAMERA_HEIGHT, this.getVertexBufferObjectManager());
		this.mGradient.setGradient(Color.WHITE, Color.BLACK, 0, 1);
		this.mGradient.setGradientFitToBounds(true);
		this.mGradient.setGradientDitherEnabled(true);
		
		
		//Parallax background
		mParallaxLayer.attachParallaxEntity(new ParallaxLayer.ParallaxEntity(0.0f, mGradient, false));
		
		new Background(this.mParallaxLayerMidTextureRegion, vertexBufferObjectManager, -5f, this.mParallaxLayer);
		new Background(this.mParallaxLayerFrontTextureRegion, vertexBufferObjectManager, -3f, this.mParallaxLayer);
		new Background(this.mParallaxLayerGroundTextureRegion, vertexBufferObjectManager, 0f, this.mParallaxLayer);
		
		scene.attachChild(mParallaxLayer);
				
		final float wheelCenterX = this.mWheelTextureRegion.getWidth() / 2 + 50;
		final float wheelCenterY = this.mWheelBaseTextureRegion.getHeight() + this.mParallaxLayerGroundTextureRegion.getHeight();
		
		// create hamster
		final float hamsterX0 = wheelCenterX;
		final float hamsterY0 = wheelCenterY - this.mWheelTextureRegion.getHeight() / 2 + 75;

		final float baseHeight = this.mWheelBaseTextureRegion.getHeight()/2 + this.mParallaxLayerGroundTextureRegion.getHeight();
		BaseInanimatedPhysicsObject wheelBase = new BaseInanimatedPhysicsObject(scene,
				wheelCenterX, baseHeight,
				this.mWheelBaseTextureRegion, vertexBufferObjectManager, ShapeType.Box, pPhysicsWorld, 
				BodyType.StaticBody ,PhysicsFactory.createFixtureDef(0f, 0f, 0f));
		 
		final float wheelY = this.mWheelBaseTextureRegion.getHeight() + this.mParallaxLayerGroundTextureRegion.getHeight();
		Wheel w = new Wheel(scene, wheelCenterX, wheelY, 
				this.mWheelTextureRegion, vertexBufferObjectManager, pPhysicsWorld, 
				PhysicsFactory.createFixtureDef(100f, 0.8f, 20.2f), wheelBase);

		//Physics for ground
		Line pLine = new Line(wheelCenterX, this.mParallaxLayerGroundTextureRegion.getHeight(), LEVEL_WIDTH, this.mParallaxLayerGroundTextureRegion.getHeight(), 0, vertexBufferObjectManager);
		PhysicsFactory.createLineBody(hPhysicsWorld, pLine, PhysicsFactory.createFixtureDef(0.0f, 0.0f, 3.0f));
		hamster = new Hamster(hamsterX0, hamsterY0, this.mHamsterTextureRegion, 
				vertexBufferObjectManager, this.hPhysicsWorld, PhysicsFactory.createFixtureDef(100f, 0.5f, 0.05f), this.camera, this, scene);
		w.setHamster(hamster);
		this.mEngine.registerUpdateHandler(new Updater(hamster, this.mWheelBaseTextureRegion.getHeight() / 2, new Vector2(wheelCenterX, wheelY), this));
		return scene;
	}
		

}

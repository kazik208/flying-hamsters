package com.codeer.flyinghamsters.views;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.codeer.flyinghamsters.views.ParallaxLayer.ParallaxEntity;

public class Background extends Sprite {
	public Background(ITextureRegion t, VertexBufferObjectManager vbom, float pParallaxFactor, ParallaxLayer p) {
		super(0, 0, t, vbom);
		this.setOffsetCenter(0, 0);
		p.attachParallaxEntity(new ParallaxEntity(pParallaxFactor, this, true));
	}
}

package com.codeer.flyinghamsters.views;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

public class Wheel {
	private WheelSprite s;
	private Body b;
	public void setHamster(Hamster h) {
		this.s.setHamster(h);
	}
	public Wheel(Scene sc, float pX, float pY, ITextureRegion itr, VertexBufferObjectManager vbom,
			PhysicsWorld phys, FixtureDef fxdef, BaseInanimatedPhysicsObject base) {
		this.s = new WheelSprite(pX, pY, itr, vbom);
		final float WHEEL_DAMPING = 0.5f;
		s.setRotationCenter(0.5f, 0.5f);
		sc.registerTouchArea(s);
		this.b = PhysicsFactory.createCircleBody(phys, this.s, BodyType.DynamicBody, fxdef);
		phys.registerPhysicsConnector(new PhysicsConnector(this.s, this.b, true, true));
		this.b.setAngularDamping(WHEEL_DAMPING);
		this.s.setUserData(this.b);
		final RevoluteJointDef pJointWheelBase = new RevoluteJointDef();
		base.connectBodies(pJointWheelBase, this.b, this.b.getWorldCenter());
		pJointWheelBase.enableMotor = false;
		phys.createJoint(pJointWheelBase);
		sc.attachChild(s);
	}
}

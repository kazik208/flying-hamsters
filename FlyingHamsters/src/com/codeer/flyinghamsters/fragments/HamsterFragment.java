package com.codeer.flyinghamsters.fragments;

import com.codeer.flyinghamsters.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HamsterFragment extends Fragment {
	
	View view;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	    view = inflater.inflate(R.layout.fragment_hamster, container, false);
	    	    
	    return view;
	  }

}

package com.codeer.flyinghamsters;	

import com.codeer.flyinghamsters.views.LevelActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ActivityMain extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        setContentView(R.layout.activity_main);
        
        //config_db = new ConfigSQLiteHelper(this);
        
        
		
    }
    
    public void onClick(final View v) {
    	
    	Intent i;
    	
        int id = v.getId();
        switch (id){
        	case R.id.PlayButton1:
				i = new Intent(v.getContext(), LevelActivity.class);
				startActivity(i);
				break;
        	case R.id.PlayButton6:
        		i = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.Kazana_uri)));
        		startActivity(i);
        		break;
        	default:
        		Toast.makeText(v.getContext(), getString(R.string.not_implemented), Toast.LENGTH_SHORT).show();
        }
    }

    
}
